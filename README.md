# php-extended/php-api-net-fakemail-object

A php API wrapper to connect to fakemail.net API

![coverage](https://gitlab.com/php-extended/php-api-net-fakemail-object/badges/master/pipeline.svg?style=flat-square)
![build status](https://gitlab.com/php-extended/php-api-net-fakemail-object/badges/master/coverage.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar php-extended/php-api-net-fakemail-object ^8`


## Basic Usage

For the basic functions, use :

```php

use PhpExtended\FakeMailNetApi\FakeMailNetApiEndpoint;
use PhpExtended\Endpoint\HttpEndpoint;

/* @var $client \Psr\Http\Client\ClientInterface */

$endpoint = new FakeMailNetApiEndpoint(new HttpEndpoint($client));

$emailAddress = $endpoint->provideNewEmail();

$metadata = $endpoint->getEmailMetadatas($emailAddress, 1); // returns an PagedIterator of FakeMailNetApiEmailMetadata

$endpoint->getEmail($metadata);	 // returns a \Psr\Http\Message\ResponseInterface 

```


## License

MIT (See [license file](LICENSE)).
