<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-net-fakemail-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiNetFakemail;

use PhpExtended\Email\EmailAddressInterface;
use Stringable;

/**
 * ApiNetFakemailEmailResponse class file.
 * 
 * This is the response to the initial request of data from the fakemail api.
 * This object is made for internal use and supposed to be reified only.
 * 
 * @psalm-suppress MissingConstructor
 */
class ApiNetFakemailEmailResponse implements Stringable
{

	/**
	 * The email address that was created.
	 * 
	 * @var EmailAddressInterface
	 */
	public EmailAddressInterface $email;

	/**
	 * Some type of code, i guess ?
	 * 
	 * @var string
	 */
	public string $heslo;

	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}

}
