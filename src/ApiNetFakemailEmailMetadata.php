<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-net-fakemail-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiNetFakemail;

use DateTimeInterface;
use PhpExtended\Email\EmailAddressInterface;

/**
 * ApiNetFakemailEmailMetadata class file.
 * 
 * This is a simple implementation of the ApiNetFakemailEmailMetadataInterface.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74ClassMetadata
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.ShortVariable")
 */
class ApiNetFakemailEmailMetadata implements ApiNetFakemailEmailMetadataInterface
{
	
	/**
	 * The id of this email.
	 * 
	 * @var int
	 */
	protected int $_id;
	
	/**
	 * The reception email address.
	 * 
	 * @var EmailAddressInterface
	 */
	protected EmailAddressInterface $_email;
	
	/**
	 * Since when this email arrived.
	 * 
	 * @var DateTimeInterface
	 */
	protected DateTimeInterface $_since;
	
	/**
	 * The sender of the mail.
	 * 
	 * @var EmailAddressInterface
	 */
	protected EmailAddressInterface $_from;
	
	/**
	 * The level of the mail.
	 * 
	 * @var string
	 */
	protected string $_level;
	
	/**
	 * The object of the mail.
	 * 
	 * @var string
	 */
	protected string $_object;
	
	/**
	 * The headline of the mail.
	 * 
	 * @var string
	 */
	protected string $_headline;
	
	/**
	 * The html line to display this metadata.
	 * 
	 * @var string
	 */
	protected string $_data;
	
	/**
	 * Constructor for ApiNetFakemailEmailMetadata with private members.
	 * 
	 * @param int $id
	 * @param EmailAddressInterface $email
	 * @param DateTimeInterface $since
	 * @param EmailAddressInterface $from
	 * @param string $level
	 * @param string $object
	 * @param string $headline
	 * @param string $data
	 */
	public function __construct(int $id, EmailAddressInterface $email, DateTimeInterface $since, EmailAddressInterface $from, string $level, string $object, string $headline, string $data)
	{
		$this->setId($id);
		$this->setEmail($email);
		$this->setSince($since);
		$this->setFrom($from);
		$this->setLevel($level);
		$this->setObject($object);
		$this->setHeadline($headline);
		$this->setData($data);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Sets the id of this email.
	 * 
	 * @param int $id
	 * @return ApiNetFakemailEmailMetadataInterface
	 */
	public function setId(int $id) : ApiNetFakemailEmailMetadataInterface
	{
		$this->_id = $id;
		
		return $this;
	}
	
	/**
	 * Gets the id of this email.
	 * 
	 * @return int
	 */
	public function getId() : int
	{
		return $this->_id;
	}
	
	/**
	 * Sets the reception email address.
	 * 
	 * @param EmailAddressInterface $email
	 * @return ApiNetFakemailEmailMetadataInterface
	 */
	public function setEmail(EmailAddressInterface $email) : ApiNetFakemailEmailMetadataInterface
	{
		$this->_email = $email;
		
		return $this;
	}
	
	/**
	 * Gets the reception email address.
	 * 
	 * @return EmailAddressInterface
	 */
	public function getEmail() : EmailAddressInterface
	{
		return $this->_email;
	}
	
	/**
	 * Sets since when this email arrived.
	 * 
	 * @param DateTimeInterface $since
	 * @return ApiNetFakemailEmailMetadataInterface
	 */
	public function setSince(DateTimeInterface $since) : ApiNetFakemailEmailMetadataInterface
	{
		$this->_since = $since;
		
		return $this;
	}
	
	/**
	 * Gets since when this email arrived.
	 * 
	 * @return DateTimeInterface
	 */
	public function getSince() : DateTimeInterface
	{
		return $this->_since;
	}
	
	/**
	 * Sets the sender of the mail.
	 * 
	 * @param EmailAddressInterface $from
	 * @return ApiNetFakemailEmailMetadataInterface
	 */
	public function setFrom(EmailAddressInterface $from) : ApiNetFakemailEmailMetadataInterface
	{
		$this->_from = $from;
		
		return $this;
	}
	
	/**
	 * Gets the sender of the mail.
	 * 
	 * @return EmailAddressInterface
	 */
	public function getFrom() : EmailAddressInterface
	{
		return $this->_from;
	}
	
	/**
	 * Sets the level of the mail.
	 * 
	 * @param string $level
	 * @return ApiNetFakemailEmailMetadataInterface
	 */
	public function setLevel(string $level) : ApiNetFakemailEmailMetadataInterface
	{
		$this->_level = $level;
		
		return $this;
	}
	
	/**
	 * Gets the level of the mail.
	 * 
	 * @return string
	 */
	public function getLevel() : string
	{
		return $this->_level;
	}
	
	/**
	 * Sets the object of the mail.
	 * 
	 * @param string $object
	 * @return ApiNetFakemailEmailMetadataInterface
	 */
	public function setObject(string $object) : ApiNetFakemailEmailMetadataInterface
	{
		$this->_object = $object;
		
		return $this;
	}
	
	/**
	 * Gets the object of the mail.
	 * 
	 * @return string
	 */
	public function getObject() : string
	{
		return $this->_object;
	}
	
	/**
	 * Sets the headline of the mail.
	 * 
	 * @param string $headline
	 * @return ApiNetFakemailEmailMetadataInterface
	 */
	public function setHeadline(string $headline) : ApiNetFakemailEmailMetadataInterface
	{
		$this->_headline = $headline;
		
		return $this;
	}
	
	/**
	 * Gets the headline of the mail.
	 * 
	 * @return string
	 */
	public function getHeadline() : string
	{
		return $this->_headline;
	}
	
	/**
	 * Sets the html line to display this metadata.
	 * 
	 * @param string $data
	 * @return ApiNetFakemailEmailMetadataInterface
	 */
	public function setData(string $data) : ApiNetFakemailEmailMetadataInterface
	{
		$this->_data = $data;
		
		return $this;
	}
	
	/**
	 * Gets the html line to display this metadata.
	 * 
	 * @return string
	 */
	public function getData() : string
	{
		return $this->_data;
	}
	
}
