<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-net-fakemail-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\ApiNetFakemail\ApiNetFakemailEndpoint;
use PhpExtended\Email\EmailAddressInterface;
use PhpExtended\HttpMessage\Response;
use PhpExtended\HttpMessage\StringStream;
use PHPUnit\Framework\TestCase;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * FakeMailNetApiEndpointTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiNetFakemail\ApiNetFakemailEndpoint
 *
 * @internal
 *
 * @small
 */
class ApiNetFakemailEndpointTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiNetFakemailEndpoint
	 */
	protected ApiNetFakemailEndpoint $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testProvideNewEmail() : void
	{
		$this->assertInstanceOf(EmailAddressInterface::class, $this->_object->provideNewEmail());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$client = new class() implements ClientInterface
		{
			
			private $_phpsessid;
			
			public function sendRequest(RequestInterface $request) : ResponseInterface
			{
				$headers = [];
				
				if(null !== $this->_phpsessid)
				{
					$request = $request->withAddedHeader('Cookie', 'PHPSESSID='.$this->_phpsessid);
				}
				
				foreach($request->getHeaders() as $headerKey => $headerArr)
				{
					$headers[] = $headerKey.': '.\implode(', ', $headerArr);
				}
				
				$http = [
					'method' => $request->getMethod(),
					'header' => \implode("\r\n", $headers)."\r\n",
					'content' => $request->getBody()->__toString(),
					'ignore_errors' => true,
					'follow_location' => 0,
				];
				$ssl = [
					'verify_peer' => false,
				];
				
				// \var_dump($http);
				
				$context = \stream_context_create(['http' => $http, 'ssl' => $ssl]);
				
				$http_response_header = [];
				$data = (string) \file_get_contents($request->getUri()->__toString(), false, $context);
				
				// \var_dump($http_response_header);
				// \var_dump($data);
				
				$response = new Response();
				
				foreach($http_response_header as $header)
				{
					$matches = [];
					if(\preg_match('#^(.*): (.*)$#', $header, $matches))
					{
						$matches2 = [];
						if(\preg_match('#PHPSESSID=(.*);#', $matches[2], $matches2))
						{
							$this->_phpsessid = $matches2[1];
						}
						
						$response = $response->withAddedHeader($matches[1], $matches[2]);
					}
				}
				
				return $response->withBody(new StringStream($data));
			}
			
		};
		
		$this->_object = new ApiNetFakemailEndpoint($client);
	}
	
}
