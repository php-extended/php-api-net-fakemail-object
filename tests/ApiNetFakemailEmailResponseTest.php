<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-net-fakemail-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\ApiNetFakemail\ApiNetFakemailEmailResponse;
use PHPUnit\Framework\TestCase;

/**
 * ApiNetFakemailEmailResponseTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiNetFakemail\ApiNetFakemailEmailResponse
 * @internal
 * @small
 */
class ApiNetFakemailEmailResponseTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiNetFakemailEmailResponse
	 */
	protected ApiNetFakemailEmailResponse $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ApiNetFakemailEmailResponse();
	}
	
}
