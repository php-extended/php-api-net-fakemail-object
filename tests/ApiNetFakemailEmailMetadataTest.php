<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-net-fakemail-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiNetFakemail\Test;

use DateTimeImmutable;
use PhpExtended\ApiNetFakemail\ApiNetFakemailEmailMetadata;
use PhpExtended\Email\EmailAddressParser;
use PHPUnit\Framework\TestCase;

/**
 * ApiNetFakemailEmailMetadataTest test file.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74TestMetadata
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiNetFakemail\ApiNetFakemailEmailMetadata
 * @internal
 * @small
 */
class ApiNetFakemailEmailMetadataTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiNetFakemailEmailMetadata
	 */
	protected ApiNetFakemailEmailMetadata $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetId() : void
	{
		$this->assertEquals(12, $this->_object->getId());
		$expected = 25;
		$this->_object->setId($expected);
		$this->assertEquals($expected, $this->_object->getId());
	}
	
	public function testGetEmail() : void
	{
		$this->assertEquals((new EmailAddressParser())->parse('test@example.com'), $this->_object->getEmail());
		$expected = (new EmailAddressParser())->parse('admin@example.com');
		$this->_object->setEmail($expected);
		$this->assertEquals($expected, $this->_object->getEmail());
	}
	
	public function testGetSince() : void
	{
		$this->assertEquals(DateTimeImmutable::createFromFormat('!Y-m-d H:i:s', '2000-01-01 00:00:01'), $this->_object->getSince());
		$expected = DateTimeImmutable::createFromFormat('!Y-m-d H:i:s', '2001-01-01 00:00:01');
		$this->_object->setSince($expected);
		$this->assertEquals($expected, $this->_object->getSince());
	}
	
	public function testGetFrom() : void
	{
		$this->assertEquals((new EmailAddressParser())->parse('test@example.com'), $this->_object->getFrom());
		$expected = (new EmailAddressParser())->parse('admin@example.com');
		$this->_object->setFrom($expected);
		$this->assertEquals($expected, $this->_object->getFrom());
	}
	
	public function testGetLevel() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getLevel());
		$expected = 'qsdfghjklm';
		$this->_object->setLevel($expected);
		$this->assertEquals($expected, $this->_object->getLevel());
	}
	
	public function testGetObject() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getObject());
		$expected = 'qsdfghjklm';
		$this->_object->setObject($expected);
		$this->assertEquals($expected, $this->_object->getObject());
	}
	
	public function testGetHeadline() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getHeadline());
		$expected = 'qsdfghjklm';
		$this->_object->setHeadline($expected);
		$this->assertEquals($expected, $this->_object->getHeadline());
	}
	
	public function testGetData() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getData());
		$expected = 'qsdfghjklm';
		$this->_object->setData($expected);
		$this->assertEquals($expected, $this->_object->getData());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ApiNetFakemailEmailMetadata(12, (new EmailAddressParser())->parse('test@example.com'), DateTimeImmutable::createFromFormat('!Y-m-d H:i:s', '2000-01-01 00:00:01'), (new EmailAddressParser())->parse('test@example.com'), 'azertyuiop', 'azertyuiop', 'azertyuiop', 'azertyuiop');
	}
	
}
