<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-net-fakemail-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiNetFakemail;

use ArrayIterator;
use InvalidArgumentException;
use PhpExtended\Email\EmailAddressInterface;
use PhpExtended\HttpMessage\RequestFactory;
use PhpExtended\HttpMessage\StreamFactory;
use PhpExtended\HttpMessage\UriFactory;
use PhpExtended\Iterator\PagedIterator;
use PhpExtended\Iterator\PagedIteratorInterface;
use PhpExtended\Parser\ParserInterface;
use PhpExtended\Parser\ParseThrowable;
use PhpExtended\Reifier\MissingParserThrowable;
use PhpExtended\Reifier\ReificationThrowable;
use PhpExtended\Reifier\Reifier;
use PhpExtended\Reifier\ReifierInterface;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestFactoryInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamFactoryInterface;
use Psr\Http\Message\UriFactoryInterface;
use RuntimeException;
use Throwable;

/**
 * ApiNetFakemailEndpoint class file.
 * 
 * This class represents the endpoint for all fakemail.net accounts.
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.CouplingBetweenObjects")
 */
class ApiNetFakemailEndpoint implements ApiNetFakemailEndpointInterface
{
	
	/**
	 * The hostname of fakemail.net.
	 * 
	 * @var string
	 */
	public const HOST = 'https://www.fakemail.net/';
	
	/**
	 * The http client.
	 *
	 * @var ClientInterface
	 */
	protected ClientInterface $_httpClient;
	
	/**
	 * The uri factory.
	 *
	 * @var UriFactoryInterface
	 */
	protected UriFactoryInterface $_uriFactory;
	
	/**
	 * The request factory.
	 *
	 * @var RequestFactoryInterface
	 */
	protected RequestFactoryInterface $_requestFactory;
	
	/**
	 * The stream factory.
	 *
	 * @var StreamFactoryInterface
	 */
	protected StreamFactoryInterface $_streamFactory;
	
	/**
	 * The reifier.
	 *
	 * @var ReifierInterface
	 */
	protected ReifierInterface $_reifier;
	
	/**
	 * Builds a new Endpoint with its depedancies.
	 * 
	 * @param ClientInterface $client
	 * @param UriFactoryInterface $uriFactory
	 * @param RequestFactoryInterface $requestFactory
	 * @param StreamFactoryInterface $streamFactory
	 * @param ReifierInterface $reifier
	 */
	public function __construct(
		ClientInterface $client,
		?UriFactoryInterface $uriFactory = null,
		?RequestFactoryInterface $requestFactory = null,
		?StreamFactoryInterface $streamFactory = null,
		?ReifierInterface $reifier = null
	) {
		$this->_httpClient = $client;
		$this->_uriFactory = $uriFactory ?? new UriFactory();
		$this->_requestFactory = $requestFactory ?? new RequestFactory();
		$this->_streamFactory = $streamFactory ?? new StreamFactory();
		$this->_reifier = $reifier ?? new Reifier();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiNetFakemail\ApiNetFakemailEndpointInterface::provideNewEmail()
	 * @throws RuntimeException
	 */
	public function provideNewEmail() : EmailAddressInterface
	{
		try
		{
			// {{{ trigger cookies
			$uri = $this->_uriFactory->createUri(self::HOST);
			$request = $this->_requestFactory->createRequest('GET', $uri);
			$response = $this->_httpClient->sendRequest($request);
			// }}}
			$responseData = $response->getBody()->getContents();
			
			$match = [];
			if(!\preg_match('#CSRF="(\\w+)"#', $responseData, $match))
			{
				throw new RuntimeException('Failed to find CSRF token into web page javascript at '.self::HOST);
			}
			/** @psalm-suppress PossiblyUndefinedIntArrayOffset */
			$csrfToken = $match[1];
			
			$uri = $this->_uriFactory->createUri(self::HOST.'index/index')->withQuery(\http_build_query(['csrf_token' => $csrfToken]));
			$request = $this->_requestFactory->createRequest('GET', $uri);
			$request = $request->withAddedHeader('X-Requested-With', 'XMLHttpRequest');
			$response = $this->_httpClient->sendRequest($request);
			// dont forget to remove utf8 bom header
			$responseBody = \trim(\str_replace("\xef\xbb\xbf", '', $response->getBody()->getContents()));
			

			/** @var array<string, string>|false $jsonResponseBody */
			$jsonResponseBody = \json_decode($responseBody, true);
			if(!\is_array($jsonResponseBody))
			{
				throw new RuntimeException('Failed to parse JSON response at '.$uri->__toString().' ('.((string) \json_last_error()).' : '.\json_last_error_msg().') from raw data :: "">'.$responseBody.'<""'."\n".\var_export($jsonResponseBody, true));
			}

			$jsonResponseObj = $this->_reifier->reify(ApiNetFakemailEmailResponse::class, $jsonResponseBody);

			return $jsonResponseObj->email;
		}
		catch(Throwable $exc)
		{
			$message = 'Failed to query email address at url {url}';
			$context = ['{url}' => self::HOST.'index/index'];
			
			throw new RuntimeException(\strtr($message, $context), -1, $exc);
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiNetFakemail\ApiNetFakemailEndpointInterface::getEmailMetadatas()
	 * @throws ParseThrowable should not happen
	 * @throws ReificationThrowable
	 * @throws RuntimeException
	 */
	public function getEmailMetadatas(EmailAddressInterface $email, int $page = 1) : PagedIteratorInterface
	{
		try
		{
			$uri = $this->_uriFactory->createUri(self::HOST.'/index/refresh?page='.((string) $page));
			$request = $this->_requestFactory->createRequest('GET', $uri);
			$request = $request->withAddedHeader('Cookie', 'TMA='.\urlencode($email->getCanonicalRepresentation()));
			$request = $request->withAddedHeader('X-Requested-With', 'XMLHttpRequest');
			$response = $this->_httpClient->sendRequest($request);
		}
		catch(Throwable $exc)
		{
			$message = 'Failed to query email address metadata for email {url}';
			$context = ['{url}' => $email->getCanonicalRepresentation()];
			
			throw new RuntimeException(\strtr($message, $context), -1, $exc);
		}
		
		$bodystr = $response->getBody()->__toString();
		$bodystr = \str_replace("\xef\xbb\xbf", '', $bodystr);	// UTF8 BOM
		$json = \json_decode($bodystr, true);
		if(false === $json || null === $json)
		{
			$message = 'Failed to get email information from url "{url}".';
			$context = ['{url}' => $uri->__toString()];
			
			throw new RuntimeException(\strtr($message, $context));
		}
		
		/** @var array<integer, ApiNetFakemailEmailMetadataInterface> $objects */
		$objects = [];
		
		foreach($json as $values)
		{
			/** @psalm-suppress MixedArrayAssignment */
			$values['email'] = $email;
			/** @psalm-suppress MixedArgument */
			$objects[] = $this->_reifier->reify(ApiNetFakemailEmailMetadata::class, $values);
		}
		
		return new PagedIterator(new ArrayIterator($objects), $page, 1);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiNetFakemail\ApiNetFakemailEndpointInterface::getEmail()
	 * @throws InvalidArgumentException
	 * @throws ParseThrowable should not happen
	 * @throws RuntimeException
	 */
	public function getEmail(ApiNetFakemailEmailMetadataInterface $metadata) : ResponseInterface
	{
		try
		{
			$uri = $this->_uriFactory->createUri(self::HOST.'/email/id/'.\urlencode((string) $metadata->getId()));
			$request = $this->_requestFactory->createRequest('GET', $uri);
			$request = $request->withAddedHeader('Cookie', 'TMA='.\urlencode($metadata->getEmail()->getCanonicalRepresentation()));
			$response = $this->_httpClient->sendRequest($request);
		}
		catch(Throwable $exc)
		{
			$message = 'Failed to query full email for id {url}';
			$context = ['{url}' => (string) $metadata->getId()];
			
			throw new RuntimeException(\strtr($message, $context), -1, $exc);
		}
		
		// always assume fakemail.net gives the GMT date
		// https://stackoverflow.com/questions/21120882/the-date-time-format-used-in-http-headers
		// https://tools.ietf.org/html/rfc7231#page-65
		try
		{
			$response = $response->withAddedHeader('Date-Email', $metadata->getSince()->format('D, d M Y H:i:s \\G\\M\\T'));
		}
		catch(InvalidArgumentException $exc)
		{
			// nothing to do
		}
		
		try
		{
			$response = $response->withAddedHeader('From', $metadata->getFrom()->__toString());
		}
		catch(InvalidArgumentException $exc)
		{
			// nothing to do
		}
		
		try
		{
			$response = $response->withAddedHeader('To', $metadata->getEmail()->__toString());
		}
		catch(InvalidArgumentException $exc)
		{
			// nothing to do
		}
		
		return $response;
	}
	
	/**
	 * Gets the email address parser.
	 * 
	 * @return ParserInterface<EmailAddressInterface>
	 * @throws MissingParserThrowable
	 */
	protected function getParser() : ParserInterface
	{
		return $this->_reifier->getConfiguration()->getParser(EmailAddressInterface::class);
	}
	
}
